
package es.ahs.testactivemq.card;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlRegistry;
import java.util.Random;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the es.ahs.testactivemq.card package.
 * <p>An CardFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class CardFactory {
    private static final Logger log = LoggerFactory.getLogger(CardFactory.class);

    private static final String[] EXPIRE_DATES =
            {"10/18", "11/20", "02/18", "12/23", "10/30", "07/17", "09/19"};
    private static final String[] FIRST_NAMES =
            {"Anton", "Elena", "Olga", "Ivan", "Semen", "Alexandr", "Vladimir"};
    private static final String[] LAST_NAMES =
            {"Frenades", "Obama", "Alonso", "Hakkinen", "Massa", "Herrero", "Onopko"};
    private static final String[] NUMBERS = { "0101 4678 2135 9071",
                                "9742 3458 3232 0972",
                                "3297 2487 2476 9893",
                                "1135 4457 6769 8794",
                                "3367 7808 6953 2355",
                                "2257 4567 0001 3336",
                                "1324 8931 3235 2217" };
    private static final String STATUS_NEW = "new";

    /**
     * Create a new CardFactory that can be used to create new instances of schema derived classes for package: es.ahs.testactivemq.card
     */
    public CardFactory() {
    }

    /**
     * Create an instance of {@link Card }
     */
    public Card createCard() {
        return new Card();
    }

    public Card generateCard(int id) {
        return new Card(id, gen(EXPIRE_DATES),
                gen(FIRST_NAMES), gen(LAST_NAMES),
                gen(NUMBERS), STATUS_NEW, 0);
    }

    private String gen(String [] array) {
        int max = array.length - 1;
        return array[new Random().nextInt(max)];
    }
}
