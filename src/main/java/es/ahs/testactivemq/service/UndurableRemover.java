package es.ahs.testactivemq.service;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.utils.ApprovedCardsMap;
import es.ahs.testactivemq.utils.CardConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.JmsUtils;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created by ahs on 10.07.16.
 */

@Service
public class UndurableRemover implements MessageListener {

    @Autowired
    CardConverter converter;

    @Autowired
    ApprovedCardsMap approvedCardsMap;

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                Card card = converter.xmlString2card(message);
                if ("remove".equals(card.getStatus())) {
                    approvedCardsMap.getCardsMap().remove(card.getId());
                }
            } catch (JMSException e) {
                JmsUtils.convertJmsAccessException(e);
            }
        }

    }
}
