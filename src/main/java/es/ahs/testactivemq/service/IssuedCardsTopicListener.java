package es.ahs.testactivemq.service;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.utils.ApprovedCardsMap;
import es.ahs.testactivemq.utils.CardConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.JmsUtils;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created by ahs on 08.07.16.
 */
@Service
public class IssuedCardsTopicListener implements MessageListener {
    private static final Logger log = LoggerFactory.getLogger(ApprovedCardsTopicListener.class);

    @Autowired
    Sender sender;

    @Autowired
    CardConverter converter;

    @Autowired
    ApprovedCardsMap approvedCardsMap;

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            Card card;
            try {
                card = converter.xmlString2card(message);
                log.info("TOPIC issuedCardsListener receive card with id: {}", card.getId());
                if (!"remove".equals(card.getStatus())) checkAndShowIfExistCard(card);
            } catch (JMSException e) {
                JmsUtils.convertJmsAccessException(e);
            }
        }
    }

    private void checkAndShowIfExistCard(Card card) {
        log.info("checkAndShowIfExistCard");
        if (approvedCardsMap.checkCardInMap(card)) {
            log.info("Ok. Card issued: " + card);
        } else {
            log.error("Error! This card don't have in cards list!!!! Card: {}", card);
        }
    }
}
