package es.ahs.testactivemq.service;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.utils.ApprovedCardsMap;
import es.ahs.testactivemq.utils.CardConverter;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.JmsUtils;
import org.springframework.stereotype.Service;

import javax.jms.*;

// * Created by akuznetsov on 07.07.2016.

@Service
public class ApprovedCardsTopicListener implements MessageListener {
    private static final Logger log = LoggerFactory.getLogger(ApprovedCardsTopicListener.class);

    @Autowired
    Sender sender;

    @Autowired
    CardConverter converter;

    @Autowired
    ApprovedCardsMap approvedCardsMap;

    @Override
    public void onMessage(Message message)  {
        log.info("TOPIC ApprovedCards.onMessage ");
        if (message instanceof TextMessage) {
            try {
                String cardXml = ((TextMessage) message).getText();
                log.info("TOPIC RECEIVED CARD!!! " + cardXml.substring(56, 61) + cardXml.substring(64, 67) + " ");
                Card card;
                card = converter.xmlString2card(message);
                if (!"remove".equals(card.getStatus())) card.setStatus("issued");
                checkPostprocessException(message, card);
                checkCardFields(card);
                log.info("Card (id: {}) issued and saved to map", card.getId());
                saveToCards(card);
                String cardReady = null;
                cardReady = converter.card2xmlString(card);
//                System.out.println(cardReady);
                Topic issuedTopic = new ActiveMQTopic("issuedCards");
                sender.send(issuedTopic, cardReady);
            } catch (JMSException e) {
                log.warn(e.getLocalizedMessage());
                throw JmsUtils.convertJmsAccessException(e);
            }
        }
    }

    private void saveToCards(Card card) {
        log.info("Save card to map....");
        approvedCardsMap.putNewCard(card);
    }

    private void checkCardFields(Card card) throws JMSException {
        if  (!hasDataIntString(card.getFirstName())
                && hasDataIntString(card.getLastName())
                && hasDataIntString(card.getNumber())
                && hasDataIntString(card.getStatus())
                && card.getStatus().equals("issued")) throw new RuntimeException("Error in card fields!");
    }

    private boolean hasDataIntString(String str) {
        if (str == null || str.equals("")) return false;
        return true;
    }

    /**
     * Execution failure after saving the message to the DB
     *
     * @param message
     * @param card
     * @throws JMSException
     */
    private void checkPostprocessException(Message message, Card card) throws JMSException {
        if (card.getId() == 5 && message.getIntProperty("JMSXDeliveryCount") < 3) {
            log.error("Card with id=5 received");
            System.out.println("Received card 5");
            System.out.println("It's " + message.getIntProperty("JMSXDeliveryCount") + " try from 3");
            throw new RuntimeException("error after processing message");
        }
    }
}
