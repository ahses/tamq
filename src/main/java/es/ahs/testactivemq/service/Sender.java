package es.ahs.testactivemq.service;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.utils.CardConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.*;
import javax.jms.Message;

/**
 * Created by akuznetsov on 06.07.2016.
 */
@Service
public class Sender {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    @Qualifier("jmsTemplate")
    JmsTemplate queueTemplate;

    @Autowired
    CardConverter converter;

    /**
     * Отправляет карту в очередь с преобразованием объекта Card в xml
     * @param dest destination queue
     * @param card card to send
     */
    public void send(final Destination dest, Card card) {
        send(dest, converter.card2xmlString(card));
    }

    public void send(final Destination dest, String text) {
        queueTemplate.send(dest, session -> {
            Message message = session.createTextMessage(text);
            return message;
        });
    }
}
