package es.ahs.testactivemq.service;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.utils.CardConverter;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.jms.Queue;
import javax.jms.Topic;
import java.util.Random;

/**
 * Created by akuznetsov on 06.07.2016.
 */

@Service
public class QueuesReceiver {
    @Autowired
    Sender sender;

    @Autowired
    CardConverter converter;

    private static final Logger log = LoggerFactory.getLogger(QueuesReceiver.class);

    /**
     * Обработка сообщений очереди cardsQueue
     * @param msg Card in xml
     */
    public void proccessNewCard(String msg) {
        log.info("Received msg in 'Cards Queue'.");
        Card receivedCard = converter.xmlString2card(msg);
        if (receivedCard == null) {
            log.info("Can't convert msg to Card. Message body: {}", msg);
            return;
        }
//        System.out.println(receivedCard);
        if ("new".equals(receivedCard.getStatus())) {
            log.info("...is cardId: {} with status: '{}'", receivedCard.getId(), receivedCard.getStatus() );
            receivedCard.setStatus("received");
            Queue receivedQueue = new ActiveMQQueue("receivedCardsQueue");
            log.info("status changed to '{}' and send to 'Received Queue' ", receivedCard.getStatus());
            sender.send(receivedQueue, receivedCard);
        } else {
            log.error("Wrong card routing!");
        }
    }

    /**
     * Обработка сообщений очереди receivedCards
     * @param msg Card in xml
     */
    public void receivedCardsListener(String msg) {
        Card card = converter.xmlString2card(msg);
        if (card == null) {
            log.error("Can't convert msg to Card. Message body: {}", msg);
            return;
        }
        log.info("Received card for set-up limit, cardId: {} ", card.getId());
        if ("received".equals(card.getStatus())) {
            int limit = new Random().nextInt(10) * 10000;
            card.setLimit(limit);
            log.info("New limit for card with id: {} = {}.", card.getId(), card.getLimit());
            Topic approvedTopic = new ActiveMQTopic("approvedCards");
            String xml = converter.card2xmlString(card);
            log.info("Send card (id: {}) to Approved cards topic", card.getId());
            sender.send(approvedTopic, xml);
        } else log.info("Can't change limit for card with status '{}'", card.getStatus());
    }
}
