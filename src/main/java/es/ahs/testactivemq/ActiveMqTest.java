package es.ahs.testactivemq;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.card.CardFactory;
import es.ahs.testactivemq.service.Sender;
import es.ahs.testactivemq.utils.ApprovedCardsMap;
import es.ahs.testactivemq.utils.CardConverter;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by akuznetsov on 07.07.2016.
 */

/*
Задание:
        1)      Поднять MQ сервер ActiveMQ;
        2)      Определить структуру сообщения в формате XML – создать xsd;
        3)      Сгенерировать java классы из xsd;
        4)      Реализовать отправку сообщения в очередь в формате XML;
        5)      Реализовать отправку сообщения в топик в формате XML;
        6)      Реализовать чтение сообщения из очереди (валидировать сообщение по XSD);
        7)      Реализовать чтение сообщения из топика на примере двух подписчиков (Рассмотреть все вариант подписки) (валидировать сообщение по XSD);
        8)      Реализовать отправку сообщения в очередь А, сразу после получения сообщения из очереди B.
*/

@Component
public class ActiveMqTest {
    private static final Logger log = LoggerFactory.getLogger(ActiveMqTest.class);
    private static int cardCounter = 0;
    private static final int minValue = 5;
    private int q = minValue + 2; // Должно быть значение не меньше 5 + forDel
    private int forDel = 2; // Set quantity cards for remove, also this variable used for set position for first removed card

    @Autowired
    Sender sender;

    @Autowired
    ApprovedCardsMap cardsBean;

    @Autowired
    CardConverter converter;

    public static void main(String[] args) {
        log.info("start");
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring/spring-app.xml");
        ctx.refresh();
        ActiveMqTest mqTest = ctx.getBean(ActiveMqTest.class);


        try {
            DefaultMessageListenerContainer approvListener = (DefaultMessageListenerContainer) ctx.getBean("apprListener");

            /*
              Реализация п.7 ч.1 задания - чтение из топика Durable подписчиком (сначала мы его выключаем, и отправляем
            сообщения, потом включаем и он автоматически получает все что пришло пока он был выключен).
              Также сразу реализованы п.4, 5, 6, 8: сообщение попадает в очередь-1 (cardsQueue), обрабатывается и
            передается в очередь-2 (receivedCardsQueue) где продолжается обработка и далее сообщение пересылается
            в топик-1 (approvedCards) у которого используется Durable подписчик (п.7) где с сообщением ведется
            дальнейшая работа и оно после сохранения в "бд" пересылается в топик-2 (issuedCards) с unDurable подписчиком
            (но в процессе работы приложения мы его не отключаем).
              Валидация сообщения по xsd происходит при вызове метода unmmarshall у Jaxb2Marshaller'а
             */
            approvListener.stop();
            approvListener.shutdown();
            System.out.println("\nTest-1a: generate cards and send over all queues and topics to base");
            System.out.println("Approved listener is switch-off!");
            mqTest.go_one();

            System.out.println("\nTest-1b: approved listener is switch-on and receive all cards from step '1a'");
            approvListener.initialize();
            approvListener.start();
            mqTest.showCardsInBase();

            /*
              Реализация п.7 ч.2 задания - работа с unDurable подписчиком: сначала отправляем сообщение
            в топик-2 (removerCards) при отключенном подписчике, после этого подписчик подключается,
            но сообщения к нему не приходят т.к. уже потеряны. Делаем повторную отправку при включенном
            подписчике - сообщения приходят, заданные действия (удаление карт с соотв. статусом из списка) происходят.

              Реализована транзакционность при отправке сообщений, проверяется созданием исключения при
            доставке сообщения с Id=5 (для контроля процесса выводится сообщени об ошибке и номер
            попытки доставки - "пропускается" с третьей попытки)

             */
            System.out.println("\nTest-2a: switch-off undurable receiver and send cards for remove to topic");
            DefaultMessageListenerContainer removeListener = (DefaultMessageListenerContainer) ctx.getBean("removeListener");
            removeListener.stop();
            removeListener.shutdown();
            mqTest.go_two();

            System.out.println("\nTest-2b: undurable receiver switch-on, but cards for remove lost");
            removeListener.initialize();
            removeListener.start();
            mqTest.showCardsInBase();

            System.out.println("\nTest-2c:  resend cards for remove to topic with switched-on receiver");
            mqTest.go_two();

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            ctx.close();
        }
    }

    /**
     * Test 1
     * Реализация п. 4 - 8 задания
     * Происходит отправка в очередь "A" (cards queue) где сообщение (карта) обрабатывается и передается
     * в очередь "B" (received cards queue) далее происходит отправка сообщения в топик "A" (approved cards)
     * где сообщение сохраняется и пересылается в топик "B" (issued cards).
     * Для работы с топиком "А" используется устойчивый подписчик (его работа проверяется в тестах 1f, 1b
     * в методе main), с топиком "B" - неусточивый (его работа в отклбченном состоянии не проверяется, т.к.
     * это реализовано в test 2 (go_two) с топиком "C" (remover cards).
     */

    public void go_one() {
        CardFactory factory = new CardFactory();
        List<Card> generatedCards = new ArrayList<>();
        System.out.println("Generate " + q + " cards.");
        pause(3500);
            for (int i = 1; i <= q; i++) {
                generatedCards.add(factory.generateCard(i));
                cardCounter = i;
            }
            Queue cardsQueue = new ActiveMQQueue("cardsQueue");
            try {
                generatedCards.stream()
                        .forEach(c -> sender.send(cardsQueue, c));
            } catch (Exception e) {
                log.error("Error with mapping card. " + e.getLocalizedMessage());
            }
            System.out.println("All cards sends to activation and save to base");
           // pause(3000);
            Map cards = cardsBean.getCardsMap();
            System.out.println("Cards sended: " + q);
            System.out.println("Cards in base: " + cards.entrySet().size());
    }

    /**
     * Test 2
     * Реализация п.7 ч.2 задания - работа с unDurable подписчиком: сначала отправляем сообщение
     * в топик-2 (removerCards) при отключенном (в main) подписчике, после этого подписчик подключается,
     * но сообщения к нему не приходят т.к. уже потеряны. Делаем повторную отправку (опять же в main) при включенном
     * подписчике - сообщения приходят, заданные действия (удаление карт с соотв. статусом из списка) происходят.
     */
    public void go_two() {
        Map cards = cardsBean.getCardsMap();
        for (int di = ActiveMqTest.minValue; di < (ActiveMqTest.minValue + forDel); di++) {
            Card card = cardsBean.getCardById(di);
            try {
                card.setStatus("remove");
            } catch (Exception e) {
                System.out.println("Remain cards in base: " + cards.entrySet().size());
                break;
            }
        }

        Topic topic4remove = new ActiveMQTopic("removerCards");

        cards.values().forEach(c -> sender.send(topic4remove, (Card) c));

        int removedCards = q - cards.entrySet().size();

        log.info("Removed {} cards", removedCards);
        System.out.println("Cards before remove: " + q);
        System.out.println("Cards for remove: " + forDel);
        System.out.println("Remain cards: " + cards.entrySet().size());
        pause(3000);
    }


    /**
     * Отобрадает количество карт в "базе" (bean с map в которой хранятся карты)
     */
    public void showCardsInBase() {
        pause(3000);
        System.out.println("Cards in base: " + cardsBean.getCardsMap().entrySet().size());
    }

    /**
     * Show "progress bar" while thread wait
     * @param t time to wait
     */
    private void pause(int t) {
        int i = 0;
        int step = t / 30;
        try {
            while (i < t) {
                System.out.print(".");
                i += step;
                Thread.sleep(step);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("");
    }
}


