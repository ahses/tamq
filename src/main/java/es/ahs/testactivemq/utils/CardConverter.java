package es.ahs.testactivemq.utils;

import es.ahs.testactivemq.card.Card;
import es.ahs.testactivemq.service.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by akuznetsov on 07.07.2016.
 */

@Component
public class CardConverter {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    Jaxb2Marshaller marshaller;

    public String card2xmlString(Card card) {
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        marshaller.marshal(card, result);
        log.info(writer.toString());
        return writer.toString();
    }

    public Card xmlString2card(Message message) throws JMSException {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            Card card = (Card) marshaller.unmarshal(new StreamSource(new StringReader(textMessage.getText())));
            return card;
        }
        return null;

    }

    public Card xmlString2card(String str) throws XmlMappingException {
        Card card = null;
        card = (Card) marshaller.unmarshal(new StreamSource(new StringReader(str)));
        return card;
    }

    /*
// universal converter template
    public <T> T unmarshal( Class<T> docClass, InputStream inputStream )
            throws JAXBException {
        String packageName = docClass.getPackage().getName();
        JAXBContext jc = JAXBContext.newInstance( packageName );
        Unmarshaller u = jc.createUnmarshaller();
        JAXBElement<T> doc = (JAXBElement<T>)u.unmarshal( inputStream );
        return doc.getValue();
    }
*/

}
