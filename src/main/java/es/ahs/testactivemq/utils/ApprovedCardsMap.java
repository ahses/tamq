package es.ahs.testactivemq.utils;

import es.ahs.testactivemq.card.Card;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by akuznetsov on 07.07.2016.
 */
@Component
public class ApprovedCardsMap {
    private static final Logger log = LoggerFactory.getLogger(ApprovedCardsMap.class);
    private static Map<Integer, Card> cards;

    public ApprovedCardsMap() {
        cards = new ConcurrentHashMap();
    }

    /**
     * Сохраняет (или обновляет) карту в мапу
     * @param card Card
     */
    public void putNewCard(Card card) {
        if (!cards.containsKey(card.getId()) && !"remove".equals(card.getStatus())) {
            cards.put(card.getId(), card);
            log.info("...card saved to map.");
        } else {
            System.out.println("Warning! Card overwrite!");
            cards.put(card.getId(), card);
        }
    }

    /**
     * Проверяет наличие карты в мапе (по Id)
     * @param card Card
     * @return true если карта с таким Id уже есть в мапе
     */
    public boolean checkCardInMap(Card card) {
        return cards.containsKey(card.getId());
    }

    public Map getCardsMap() {
        return cards;
    }

    public Card getCardById(int id) {
        if (cards.containsKey(id)) {
            log.info("return card with id {}", id);
            return cards.get(id);
        }
        else {
            log.error("can't get card with id {}", id);
            return null;
        }
    }

}
