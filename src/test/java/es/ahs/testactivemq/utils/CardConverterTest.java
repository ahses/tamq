package es.ahs.testactivemq.utils;

import es.ahs.testactivemq.card.Card;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.XmlMappingException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.jms.JMSException;

import static org.junit.Assert.*;

/**
 * Created by akuznetsov on 12.07.2016.
 */
@ContextConfiguration({"classpath:spring/spring-app.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CardConverterTest {
    private static final String xmlTestCard_1 =
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Card><Id>33</Id><Expire>12/23</Expire><FirstName>Olga</FirstName><LastName>Fernandes</LastName><Number>9742 3458 3232 0972</Number><Status>issued</Status><Limit>80000</Limit></Card>";
    private static final String xmlWrongCard_1 =
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Carder><Id>33</Id><Expire>12/23</Expire><FirstName>Olga</FirstName><LastName>Frenades</LastName><Number>9742 3458 3232 0972</Number><Status>issued</Status><Limit>80000</Limit></Card>";

    private static final Card testCard_1 = new Card(33, "12/23", "Olga", "Fernandes", "9742 3458 3232 0972", "issued", 80000);

    @Autowired
    CardConverter converter;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void card2xmlStringTest() throws Exception {
        Assert.assertEquals(xmlTestCard_1, converter.card2xmlString(testCard_1));
    }

    @Test(expected = XmlMappingException.class)
    public void card2xmlStringTestXmlException() throws Exception {
        converter.xmlString2card(xmlWrongCard_1);
    }

    @Test
    public void xmlString2cardTest() throws Exception {
        Assert.assertEquals(testCard_1, converter.xmlString2card(xmlTestCard_1));
    }

}